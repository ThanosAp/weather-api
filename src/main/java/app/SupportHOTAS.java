package app;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import java.util.LinkedList;
import java.util.List;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.stereotype.Component;

import model.Location;

@Component
public class SupportHOTAS {

	EntityModel<Location> location(Location location, Float temperature, String units) {
		return new EntityModel<>(location,
				linkTo(methodOn(LocationController.class).location(location.getId(), temperature, units)).withSelfRel());
	}
	
	CollectionModel<EntityModel<Location>> locationList(List<Location> locations, String units, Float temperature, String locationIds){

		List<EntityModel<Location>> summary = new LinkedList<>();
		for (Location favourite : locations) {
			summary.add(location(favourite, temperature, units));
		}		

		return new CollectionModel<>(summary,
				linkTo(methodOn(LocationController.class).summary(temperature, units, locationIds)).withSelfRel());
	}

}
