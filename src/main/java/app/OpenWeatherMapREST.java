package app;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;

import model.Location;

public interface OpenWeatherMapREST {
	public ResponseEntity<CollectionModel<EntityModel<Location>>> summary(String unit, String locationIds, Float temp);
	public ResponseEntity<EntityModel<Location>> location(Long id, Float temperature, String units);
}
