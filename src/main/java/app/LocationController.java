package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import model.Location;

@RestController
public class LocationController {

	@Autowired
	private OpenWeatherMapREST proxy;

	@GetMapping("/weather/summary")
	public ResponseEntity<CollectionModel<EntityModel<Location>>> summary(
			@RequestParam(value = "temperature", required = false) Float temperature,
			@RequestParam("units") String units,
			@RequestParam("locations") String locationIds) {

		return proxy.summary(units, locationIds, temperature);
	}

	@GetMapping("/weather/locations/{location_id}")
	public ResponseEntity<EntityModel<Location>> location(
			@PathVariable Long location_id,
			@PathVariable(value = "temperature", required = false) Float temperature,
			@PathVariable(value = "units", required = false) String units) {

		return proxy.location(location_id, temperature, units);
	}

}
