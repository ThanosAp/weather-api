package app;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;

import model.Location;
import model.Summary;

public class OpenWeatherMap implements OpenWeatherMapREST {

	@Autowired
	private OpenWeatherMapAPI api;

	@Autowired
	private SupportHOTAS hotas;

	@Override
	public ResponseEntity<EntityModel<Location>> location(Long id, Float temperature, String units) {
		// TODO epestrepse json apo katw
		String locationConfiguration = api.getLocation(id, units);
		Location location = new Location(locationConfiguration);
		EntityModel<Location> locationHotas = hotas.location(location, temperature, units); 
		
		return ResponseEntity.ok(locationHotas);
	}

	@Override
	public ResponseEntity<CollectionModel<EntityModel<Location>>> summary(String units, String locationIds, Float temperature) {

		List<Location> summary = new Summary().locationsSummary(units, locationIds, temperature);
		CollectionModel<EntityModel<Location>> summaryHotas = hotas.locationList(summary, units, temperature, locationIds);

		return ResponseEntity.ok(summaryHotas);
	}

}
