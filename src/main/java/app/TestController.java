package app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class TestController {

	@Autowired
	private OpenWeatherMapProxy proxy;

	@GetMapping("/weather/test/locations/{multiplier}")
	public String location(@PathVariable int multiplier) {

		RestTemplate rest = new RestTemplate();
		for(int i=0; i<multiplier; i++ ) {			
			rest.getForObject("http://localhost:8080/weather/locations/2618425", String.class);
		}
		String reply = "Backend '/locations' was called " + proxy.getDebugLocation() + " times.";
		proxy.setDebugLocation(0);
		return reply;
	}

	@GetMapping("/weather/test/summary/{multiplier}")
	public String summary(@PathVariable int multiplier) {

		RestTemplate rest = new RestTemplate();
		for(int i=0; i<multiplier; i++ ) {			
			rest.getForObject("http://localhost:8080/weather/summary?units=celcius&locations=2618425,2619528,2624652,2615876&temperature=0", String.class);
		}
		String reply = "Backend '/summary' was called " + proxy.getDebugSummary() + " times.";
		proxy.setDebugSummary(0);
		return reply;
	}
}
