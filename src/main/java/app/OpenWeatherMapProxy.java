package app;

import org.springframework.boot.autoconfigure.cache.CacheManagerCustomizer;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.github.benmanes.caffeine.cache.CaffeineSpec;

import model.Location;

@Service
public class OpenWeatherMapProxy extends OpenWeatherMap implements CacheManagerCustomizer<CaffeineCacheManager> {

	private int debugLocation = 0;
	private int debugSummary = 0;

	@Override
	@Cacheable(value="location", key="#id")
	public ResponseEntity<EntityModel<Location>> location(Long id, Float temperature, String units) {
		setDebugLocation(getDebugLocation() + 1);
		return super.location(id, temperature, units);
	}

	@Override
	@Cacheable(value="summary", key="#locationIds")
	public ResponseEntity<CollectionModel<EntityModel<Location>>> summary(String units, String locationIds, Float temperature) {
		setDebugSummary(getDebugSummary() + 1);
		return super.summary(units, locationIds, temperature);
	}

	@Override
	public void customize(CaffeineCacheManager cacheManager) {
		cacheManager.setCaffeineSpec(CaffeineSpec.parse("maximumSize=500,expireAfterWrite=30s"));		
	}

	public int getDebugLocation() {
		return debugLocation;
	}

	public void setDebugLocation(int debugLocation) {
		this.debugLocation = debugLocation;
	}

	public int getDebugSummary() {
		return debugSummary;
	}

	public void setDebugSummary(int debugSummary) {
		this.debugSummary = debugSummary;
	}

}
