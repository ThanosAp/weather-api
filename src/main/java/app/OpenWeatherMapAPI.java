package app;

import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class OpenWeatherMapAPI {

	private static final String APPID = "&APPID=d97120e242a8305d3ac5bf1b235a9e81";
	private static final String CELCIUS = "&units=metric";
	private static final String FAHRENHEIT= "&units=metric";
	private RestTemplate restTemplate = new RestTemplate();

	public String getLocation(Long id, String units) {
		if (units == null) {			
			units = "";
		}else 
			{			
			switch (units) {
				case "celcius":
					units = CELCIUS;
					break;
				case "c":
					units = CELCIUS;
					break;
				case "fahrenheit":
					units = FAHRENHEIT;
					break;
				case "f":
					units = FAHRENHEIT;
					break;
				default:
					units = CELCIUS;
					break;
			}
		}
		String url = "http://api.openweathermap.org/data/2.5/forecast?id=" + id + units + APPID;
		return restTemplate.getForObject(url,String.class);
	}
}
