package model;

import org.springframework.stereotype.Service;

@Service
public class WeatherConditions {

	private Float temperature;
	private String time;

	public Float getTemperature() {
		return temperature;
	}

	public void setTemperature(Float temperature) {
		this.temperature = temperature;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	@Override
	public String toString() {
		return "On " + getTime() + " temperature will be " + getTemperature();
	}
}
