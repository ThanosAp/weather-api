package model;

import java.util.LinkedList;
import java.util.List;

import app.OpenWeatherMapAPI;


public class Summary {

	private OpenWeatherMapAPI api = new OpenWeatherMapAPI();

	public List<Location> locationsSummary(String units, String locationIds, Float temperature) {
		
		List<Location> favourites = new LinkedList<>();
		for (String id : locationIds.split(",")) {

			String location = api.getLocation(Long.parseLong(id), units);
			Location candidate = new Location(location);
			
			for (WeatherConditions weatherConditions : candidate.getWeatherList()) {
				if (weatherConditions.getTemperature() > temperature) {
					favourites.add(candidate);
					break;
				}
			}
		}		
		return favourites;
	}

}
