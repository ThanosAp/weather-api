package model;

import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

@Service
public class Location {
	
	private Long id;
	private String name;
	private String country;
	private List<WeatherConditions> weatherList;
	
	public Location(String configuration) {

		JsonObject locationJson        = new Gson().fromJson(configuration, JsonObject.class);
		Long       locationId          = locationJson.getAsJsonObject("city").get("id").getAsLong();
		String     locationName        = locationJson.getAsJsonObject("city").get("name").getAsString();
		String     locationCountryCode = locationJson.getAsJsonObject("city").get("country").getAsString();
		JsonArray  conditionsArray     = locationJson.getAsJsonArray("list");
		List<WeatherConditions> weatherConditionsList = new LinkedList<>();

		for (JsonElement condition : conditionsArray) {

			Float temperature = condition.getAsJsonObject().get("main").getAsJsonObject().get("temp").getAsFloat();
			String time       = condition.getAsJsonObject().get("dt_txt").getAsString();

			WeatherConditions weatherConditions = new WeatherConditions();
			weatherConditions.setTemperature(temperature);
			weatherConditions.setTime(time);
			weatherConditionsList.add(weatherConditions);
		}

		setId(locationId);
		setName(locationName);
		setCountry(locationCountryCode);
		setWeatherList(weatherConditionsList);
	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public List<WeatherConditions> getWeatherList() {
		return weatherList;
	}
	public void setWeatherList(List<WeatherConditions> weatherList) {
		this.weatherList = weatherList;
	}

}
